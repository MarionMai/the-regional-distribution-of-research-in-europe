# The regional distribution of research in Europe

This project includes the script and data used to measure the regional distribution of research in Europe with different methods and R&D indicators. Materials for the chapter "Regional Distribution of Research: The Spatial Polarisation in Question".

Data from Eurostat and the Web of Science/OST-HCERES (geocoded and processed by UMR LISST - NETSCIENCE project) - 2001-2012
Analyses carried out in February-March 2020

# Aknowledgments

I would like to thank Laurent Jégou for providing the publication data counted at the level of NUTS 0,1 and 2 (complete and normalised counting); 
Ronan Ysebaert for advising me on the use and exploration of Eurostat data; Mayline Strouk for her inspiring work on science in Svalbard; 
Michel Grossetti for discussions and advices; The Eighties group for inspiring discussions ; Nicolas Roelandt and François Briatte for R tips; 
Rafael Ball, Director ETH-Library and Collections, for his patience and encouragement during the writing of this chapter.